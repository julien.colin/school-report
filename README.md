# School Report

School Report est une application de gestion de notes d'élèves.

# Stack
CakePHP 2.9.5

PostgreSQL 9.3.6

# Fonctionnalités
Gestion des élèves

Ajout de notes par matière

# Arborescence
application : contient les sources de l'application

database : contient le script de création de la base de données

# Page d'accueil
/students