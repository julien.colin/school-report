<?php

App::uses('AppController', 'Controller');

class GradesController extends AppController {
	
    public $uses = array('Grade', 'Student');	
	public $helpers = array('Html', 'Form', 'Flash');
	public $components = array('Flash');
    

	public function add($id) {
		$student = $this->Student->findById($id);
		$this->set('student', $student);

		if ($this->request->is('post')) {
			$this->Grade->create();
			if ($this->Grade->save($this->request->data)) {
				$this->Flash->success(__('Note ajoutée.'));
				return $this->redirect(array('controller' => 'students', 'action' => 'view', $id));
			}
			$this->Flash->error(__('Erreur lors de l\'ajout de note.'));
		}
	}
	
}
