<?php

App::uses('AppController', 'Controller');

class StudentsController extends AppController {
	
	public $helpers = array('Html', 'Form', 'Flash');
	public $components = array('Flash');
	
	public function index() {
		$this->set('students', $this->Student->find('all'));
	}
	
	public function view($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Elève non trouvé'));
		}
		
		$student = $this->Student->findById($id);
		if (!$student) {
			throw new NotFoundException(__('Elève non trouvé'));
		}
		$this->set('student', $student);
	}
	
	
	public function add() {
		if ($this->request->is('post')) {
			$this->Student->create();
			if ($this->Student->save($this->request->data)) {
				$this->Flash->success(__('Elève ajouté.'));
				return $this->redirect(array('action' => 'index'));
			}
			$this->Flash->error(__('Impossible.'));
		}
	}
	
	public function edit($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Elève inconnu'));
		}
		
		$student = $this->Student->findById($id);
		if (!$student) {
			throw new NotFoundException(__('Elève inconnu'));
		}
		
		if ($this->request->is(array('student', 'put'))) {
			$this->Student->id = $id;
			if ($this->Student->save($this->request->data)) {
				$this->Flash->success(__('Mise à jour avec succès.'));
				return $this->redirect(array('action' => 'index'));
			}
			$this->Flash->error(__('Mise à jour impossible.'));
		}
		
		if (!$this->request->data) {
			$this->request->data = $student;
		}
	}
	
	public function delete($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		
		if ($this->Student->delete($id)) {
			$this->Flash->success(
			            __('Suppression OK.', h($id))
			        );
		}
		else {
			$this->Flash->error(
			            __('Erreur lors de la suppression.', h($id))
			        );
		}
		
		return $this->redirect(array('action' => 'index'));
	}
}
