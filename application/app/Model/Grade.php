<?php
App::uses('AppModel', 'Model');

class Grade extends AppModel {

    public $validate = array(
        'subject' => array(
            'rule' => 'notBlank'
        ),
        'grade' => array(
            'rule' => 'notBlank'
        )
    );
}
