<?php
App::uses('AppModel', 'Model');

class Student extends AppModel {

    public $hasMany = array(
        'Grade' => array(
            'className' => 'Grade',
            'order' => 'Grade.id ASC',
            'dependent' => true
        )
    );

    public $validate = array(
        'firstname' => array(
            'rule' => 'notBlank'
        ),
        'lastname' => array(
            'rule' => 'notBlank'
        )
    );
}
