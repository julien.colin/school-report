
<h1>Ajout d'une note à 
<?php echo $student['Student']['lastname'] . " "; echo $student['Student']['firstname']; ?></h1>
<?php
echo $this->Form->create('Grade');
echo $this->Form->input('subject', array('label' => 'Matière'));
echo $this->Form->input('grade', array('label' => 'Note',
                'empty' => 'Note...',
                'type' => 'select',
                'options' => array_combine(range(0,20,1),range(0,20,1))
            ));
echo $this->Form->input(
        'student_id',
        array('type' => 'hidden', 'value' => $student['Student']['id']));
echo $this->Form->submit('Enregistrer');
echo $this->Form->button('Annuler', array(
   'type' => 'button',
   'onclick' => 'location.href=\'/students/view/' . $student['Student']['id'] . '\''
));
echo $this->Form->end();
?>