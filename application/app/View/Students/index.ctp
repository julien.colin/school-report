<h1>Elèves</h1>
<?php echo $this->Html->link('Ajouter un élève', array('controller' => 'students', 'action' => 'add')); ?>
<table>
    <thead>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Date de naissance</th>
        <th>Détail</th>
        <th>Supprimer</th>
        <th>Modifier</th>
    </thead>

    <tbody>
        <?php foreach ($students as $student): ?>
        <tr>
            <td><?php echo $student['Student']['firstname']; ?></td>
            <td><?php echo $student['Student']['lastname']; ?></td>
            <td><?php echo date_create($student['Student']['birthdate'])->format('d-m-Y'); ?></td>
            <td><?php echo $this->Html->link('Détail',
                array('controller' => 'students', 'action' => 'view', $student['Student']['id'])); ?></td>
            <td><?php echo $this->Form->postLink(
                    'Supprimer',
                    array('action' => 'delete', $student['Student']['id']),
                    array('confirm' => 'Etes-vous sûr ?'));
                ?></td>
            <td>
                <?php echo $this->Html->link('Modifier', array('action' => 'edit', $student['Student']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php unset($student); ?>
    </tbody>   
</table>