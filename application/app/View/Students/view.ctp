<h1>Détail d'un élève</h1>

<table>
    <tr>
        <td>Nom :</td>
        <td><?php echo h($student['Student']['firstname']); ?></td>
    </tr>
    <tr>
        <td>Prénom :</td>
        <td><?php echo h($student['Student']['lastname']); ?></td>
    </tr>
    <tr>
        <td>Date de naissance :</td>
        <td><?php echo h(date_create($student['Student']['birthdate'])->format('d-m-Y')); ?></td>
    </tr>
</table>

<?php echo $this->Html->link('Ajouter une note', array('controller' => 'grades', 'action' => 'add', $student['Student']['id'])); ?>
<table>
    <thead>
        <th>Matière</th>
        <th>Note</th>
    </thead>
    <tboby>
        <?php foreach ($student['Grade'] as $grade): ?>
        <tr>
            <td><?php echo $grade['subject']; ?></td>
            <td><?php echo $grade['grade']; ?></td>
        </tr>
        <?php endforeach; ?>
        <?php unset($grade); ?>
    </tbody>
</table>

<?php echo $this->Html->link('Retour à la liste', array('controller' => 'students', 'action' => 'index')); ?>
