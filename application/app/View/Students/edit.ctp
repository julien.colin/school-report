
<h1>Edition d'un élève</h1>
<?php
echo $this->Form->create('Student');
echo $this->Form->input('firstname', array('label' => 'Nom'));
echo $this->Form->input('lastname', array('label' => 'Prénom'));
echo $this->Form->input('birthdate', array('label' => 'Date de naissance',
    'dateFormat' => 'DMY',
    'minYear' => date('Y') - 70,
    'maxYear' => date('Y') - 18,));
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->submit('Enregistrer');
echo $this->Form->button('Annuler', array(
   'type' => 'button',
   'onclick' => 'location.href=\'/students\''
));
echo $this->Form->end();
?>