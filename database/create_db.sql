CREATE TABLE students (
	id serial NOT NULL,
	firstname text,
	lastname text,
	birthdate date,
	CONSTRAINT pk_id_student PRIMARY KEY (id)
);

CREATE TABLE grades (
	id serial NOT NULL,
	subject text,
	grade int,
	student_id int,
	CONSTRAINT pk_id_grades PRIMARY KEY (id),
	CONSTRAINT fk_student_id FOREIGN KEY (student_id)
	      REFERENCES students (id) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION
);